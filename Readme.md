# Installation

```
sudo apt install docker.io
sudo apt install docker-compose
```

# Setup
```
sudo groupadd docker
sudo usermod -aG docker $USER
```

After that restart the current session

# Example of use docker commands

```
docker pull ubuntu 
```
Pull latest ubuntu image to your PC
```
docker run -it ubuntu bash 
```
Create and start ubuntu container with bash command using interactive mode and attach to console session
```
docker ps 
```
List of running containers
```
docker ps -a 
```
List of all containers)
```
docker start <container name or id> 
```
Start container
```
docker attach <container name or id> 
```
Attach to container
```
docker exec -it <container name or id> bash 
```
Start new process of bash in container and attach to him
```
docker stop <container name or id> 
```
Stop container
```
docker rm <container name or id> 
```
Remove container
```
docker rm $(docker ps -a -q) 
```
Remove all containers in system
```
docker rmi <image name or id> 
```
Remove image
```
docker rmi $(docker images -q) 
```
Remove all images
```
docker system prune 
```
Remove all unused (stoped) containers, images, build cache and networks
```
docker build 
```
Build an image from Dockerfile

# Example of use docker-compose commands
```
docker-compose up 
```
Create and start containers
```
docker-compose up -d 
```
Create and start containers and detach from console session
```
docker compose stop 
```
Stop containers
```
docker compose down 
```
Stop and remove containers, networks, images, and volumes
```
docker-compose up -d --force-recreate --build 
```
Build all images, force recreate containers, start and detach from console
```
docker-compose ps 
```
List containers
```
docker-compose top 
```
List running processes

# Links

https://hub.docker.com/ (Oficial docker repository)

https://docs.docker.com/ (Oficial docker documentation)

https://habr.com/ru/post/353238/ https://habr.com/ru/post/250469/ https://habr.com/ru/post/275513/ (Posts on habr)

https://stackoverflow.com/ :)
